# imports all functions from helpers.py
from helpers import *

# getting an ad id from user and storing it in variable ad_id

# reading the contents of ad_data_dump.csv as string
csvfile = read_file('ad_data_dump.csv')

# looks at csvfile and splits the string everytime it reads  \n
result = csvfile.split('\n')


dict_convert = convert_lines(result)
user_input = raw_input('Which adid would you like the conversion rate for? ')
for dicts in dict_convert:
    while user_input == dicts['adid']:
        print "adid:", dicts['adid'], "| conversions:", dicts['conversions'], "/", "clicks:", dicts['clicks'], "= conversion rates:", dicts['conversions'] / dicts['clicks']
        break
