def read_file(file_name):
    with open(file_name) as file:
        return file.read()




def convert_to_dict(line):
    """
    converting string to dict
    :param line: string
    :return: dict
    """
    data = line.split(',')
    dict = {
        'date': data[0],
        'adid': data[1],
        'clicks': float(data[2]),
        'conversions': float(data[3])
    }
    return dict

def convert_lines(list_of_strings):
    """
    converts list of strings into list of dicts
    :param list_of_strings: list[string]
    :return: list[dict]
    """
    new_list = []

    #
    for adding_one in list_of_strings:
        if adding_one != 'date,adid,clicks,conversions':
            d = convert_to_dict(adding_one)
            new_list.append(d)

    return new_list